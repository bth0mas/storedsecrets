
# -*- python -*- script to demo my use of secrets

import os

import storedsecrets  # mine

##----------------------------------------------------------------------------
## parse CLI arguments

import argparse

parser = argparse.ArgumentParser()

parser.add_argument('--secrets', '-S', default="99-demo.json",
                    help="the file storing secrets (default: %(default)s)")

args = parser.parse_args()

##----------------------------------------------------------------------------

my_secrets = storedsecrets.StoredSecrets(args.secrets)

s_list = my_secrets.keys()

print('List of available secrets: {}'.format(s_list))

for s in s_list:
    print('{} -> {}'.format(s, my_secrets.get(s)))
