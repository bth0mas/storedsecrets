
# -*- python -*- script to demo my use of secrets

import json
import os

# complete, good-citizen approach
# SECRET = os.path.join(os.getenv('SECRETS', os.path.expanduser('~/etc/secrets')),
#                       "99-demo.json")

# just to default read into current dir
SECRET = os.path.join(os.getenv('SECRETS', ""), "99-demo.json")

##----------------------------------------------------------------------------
## optionally: use argparse - see below if you want to get rid of this part

import argparse

parser = argparse.ArgumentParser()

# flaw: remember by default argparse is case insensitive...
parser.add_argument('--secrets', '-S', default=SECRET,
                    help="the file storing secrets (default: %(default)s)")

args = parser.parse_args()

print('file containing secrets: "{}"'.format(args.secrets))

##----------------------------------------------------------------------------
## then open and read - replace args.secrets by SECRET if you don't use argparse

try:
    with open( args.secrets ) as f:
        secrets = json.load(f)
except OSError:
    print('FYI, secret file "{}" cannot be found/read...'.format(args.secrets))
    secrets = {}

# Optionally: silently get rid of secrets meta info
secrets.pop('__meta__', None)

print("secrets:", secrets)
