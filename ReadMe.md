
Approach proposal to storing secrets outside of scripts and
development areas.

1. env variable to a common "root-of-all-secrets" directory

```shell
SECRETS=/Volumes/myVault/etc
export SECRETS
```

Reco for defaulting: `~/etc/secrets` (if we really want to be good citizen).

2. JSON files in the directory, storing the secrets, organized per
   service
   
   Comments / usage / ... to be stored within the JSON file, in a
   `__meta__` section (dict).
   
   
3. common script snippets to be reused in each languages, to be
   developped, and/or simple drop-in modules
   
   
## Demos in Python: ##

 * simple module: `storedsecrets.py`
 * example using the above module: `storedsecrets_demo_mod_json.py`
 * example of simple code snippet: `storedsecrets_demo_simple_json.py`


# ideas for the future #

Read different file formats: .ini, .yaml

Possibly read formats from `pysec`.

Read secrets from Docker secrets.



------------------------------------------------------------------------------

# Appendix - virtual env for coding purpose

```shell
python3.6 -m venv --prompt stored_secrets __venv__
source __venv__/bin/activate
pip install --upgrade pip
pip install --upgrade build
pip install --upgrade twine
```

resulting in:


```shell
(stored_secrets)> pip list
Package            Version
------------------ -----------
bleach             3.3.0
build              0.3.1.post1
certifi            2020.12.5
chardet            4.0.0
colorama           0.4.4
docutils           0.17.1
idna               2.10
importlib-metadata 4.0.1
keyring            23.0.1
packaging          20.9
pep517             0.10.0
pip                21.1.1
pkginfo            1.7.0
Pygments           2.8.1
pyparsing          2.4.7
readme-renderer    29.0
requests           2.25.1
requests-toolbelt  0.9.1
rfc3986            1.4.0
setuptools         39.0.1
six                1.15.0
toml               0.10.2
tqdm               4.60.0
twine              3.4.1
typing-extensions  3.7.4.3
urllib3            1.26.4
webencodings       0.5.1
zipp               3.4.1
```
